# Stage



## Création d'une IA pour le jeu EnGarde en Java
__Objectif__

Je vais créer trois types d'IA pour le jeu EnGarde en Java : une IA aléatoire, une IA personnalisée et une IA utilisant l'algorithme MiniMax.
Étapes à suivre

    - IA Aléatoire (Random)
        - Je vais générer des mouvements aléatoires pour l'IA.

    - IA Personnalisée
	- Je vais définir ma propre stratégie de prise de décision pour l'IA en analysant le jeu et en mettant en œuvre une logique spécifique.

    - IA avec l'algorithme MiniMax
	- Je vais implémenter l'algorithme MiniMax pour évaluer les mouvements possibles et choisir le meilleur mouvement en fonction des positions futures.

## Conclusion

Avec ces trois types d'IA en place, je pourrai les tester et les comparer pour évaluer leur efficacité dans le jeu EnGarde.
